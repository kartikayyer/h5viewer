import sys
import glob
import numpy as np
import h5py
from geometry_funcs import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

if len(sys.argv) < 2:
	print "Need file list"
	sys.exit()

class shotview(QtGui.QWidget):
	def __init__(self):
		super(shotview, self).__init__()
		
		self.current = 0
		self.b_size = 100
		self.h5path = '/data/data'
#		self.geom_fname = 'cspad-cxif5315-cxi-taw4.geom'
		self.geom_fname = 'LE79-v6.geom'
		
		self.initdata()
		self.initUI()
	
	def initdata(self):
		f = open(sys.argv[1], "r")
		self.files = f.readlines()
		f.close() 
		
		self.num_images = len(self.files)
		for i in range(self.num_images):
			self.files[i] = self.files[i].rstrip().lstrip()
		
		print "Number of frames =", self.num_images
		self.num_batches = int(self.num_images / self.b_size)
		
		yx, shape = get_ij_slab_shaped(self.geom_fname)
		self.y, self.x = yx[0], yx[1]
		
		print "Number of pixels =", len(self.y.ravel())
		
		self.images = np.zeros((self.b_size,)+shape, dtype='i2')
	
	def initUI(self):
		hbox = QtGui.QHBoxLayout()
		vbox = QtGui.QVBoxLayout()
		
		self.iw = pg.ImageView()
		self.iw.setLevels(0,4000)
		
		self.num_entry = QtGui.QLineEdit('1', self)
		self.num_entry.editingFinished.connect(self.num_batch)
		self.num_entry.resize(self.num_entry.sizeHint())
		
		next_button = QtGui.QPushButton('Next', self)
		next_button.clicked.connect(self.next_batch)
		next_button.resize(next_button.sizeHint())
		
		prev_button = QtGui.QPushButton('Previous', self)
		prev_button.clicked.connect(self.prev_batch)
		prev_button.resize(prev_button.sizeHint())
		
		rand_button = QtGui.QPushButton('Random', self)
		rand_button.clicked.connect(self.rand_batch)
		rand_button.resize(rand_button.sizeHint())
		
		self.num_label = QtGui.QLabel('%d/%d'%(self.current+1,self.num_batches), self)
		self.num_label.resize(self.num_label.sizeHint())
		
		hbox.addWidget(self.num_label)
		hbox.addStretch(1)
		hbox.addWidget(self.num_entry)
		hbox.addWidget(rand_button)
		hbox.addWidget(prev_button)
		hbox.addWidget(next_button)
		
		vbox.addWidget(self.iw)
		vbox.addLayout(hbox)
		
		self.setLayout(vbox)
		
		self.resize(800,800)
		self.show()
		
		self.get_batch(0)
	
	def get_batch(self, b_num):
		bs = self.b_size
		if b_num == self.num_batches - 1:
			bs = self.num_images - b_num*(self.num_batches-1)
		
		for i in range(bs):
			f = h5py.File(self.files[b_num*self.b_size + i],"r")
			self.images[i, self.y, self.x] = np.array(f[self.h5path]).ravel()
			f.close()
			sys.stderr.write("\rParsed frame %d/%d in batch %d/%d" %(i, bs, b_num, self.num_batches))
		
		self.iw.setImage(self.images.transpose((0,2,1)), autoLevels=False)
		self.num_label.setText('%d/%d'%(b_num+1,self.num_batches))
	
	def next_batch(self):
		self.current += 1
		if self.current > self.num_batches - 1:
			self.current = self.num_batches - 1
		self.get_batch(self.current)
	
	def prev_batch(self):
		self.current -= 1
		if self.current < 0:
			self.current = 0
		self.get_batch(self.current)
	
	def num_batch(self):
		num = int(self.num_entry.text())
		if num <= self.num_batches and num > 0 and num-1 != self.current:
			self.current = num - 1
			self.get_batch(self.current)
	
	def rand_batch(self):
		self.current = np.random.randint(0, self.num_batches)
		self.get_batch(self.current)

def main():
	app = QtGui.QApplication(sys.argv)
	sh = shotview()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
