# Various utilities to view CSPAD images from LCLS in HDF5 files

## Prerequisites

Following are the python libraries needed

 * numpy
 * h5py
 * pyqtgraph, which in turn needs pyqt4.8+

These are all available on the LCLS machines. 

## Viewer types

There are many forms of output generated by different programs. Each script 
deals with a different kind.

 * cxiview.py - View many images in a CXIDB format .cxi file
 * cheetahview.py - View a list of single-shot .h5 files (old Cheetah output)
 * shotview.py - View images from a SLAC h5 file
 * maskview.py - View pixel mask used in Cheetah and other places

