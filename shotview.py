import sys
import glob
import numpy as np
import h5py
from geometry_funcs import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

if len(sys.argv) < 2:
	print "Need h5 file"
	sys.exit()

class shotview(QtGui.QWidget):
	def __init__(self):
		super(shotview, self).__init__()
		
		self.c_size = 22
		self.current = 0
		self.c_num = 5
		self.b_size = self.c_size * self.c_num
		self.h5path = '/Configure:0000/Run:0000/CalibCycle:0000/CsPad::ElementV2/CxiDs2.0:Cspad.0/data'
		self.geom_fname = 'cspad-cxif5315-cxi-taw4.geom'
		
		self.initdata()
		self.initUI()
	
	def initdata(self):
		f = h5py.File(sys.argv[1], "r")
		self.num_images = len(f[self.h5path])
		f.close()
		
		print "Number of frames =", self.num_images
		self.num_batches = int(self.num_images / self.b_size)
		
#		self.y, self.x = make_yx_from_4_8_16_185_388(self.geom_fname)
		yx, shape = get_ij_psana_shaped(self.geom_fname)
		self.y, self.x = yx[0], yx[1]
		
		print "Number of pixels =", len(self.y.ravel())
		
#		self.raw_images = np.zeros((self.b_size, len, dtype='i2')
		self.images = np.zeros((self.b_size,)+shape, dtype='i2')
	
	def initUI(self):
		hbox = QtGui.QHBoxLayout()
		vbox = QtGui.QVBoxLayout()
		
		self.iw = pg.ImageView()
		self.iw.setLevels(0,200)
		
		self.num_entry = QtGui.QLineEdit('1', self)
		self.num_entry.editingFinished.connect(self.num_batch)
		self.num_entry.resize(self.num_entry.sizeHint())
		
		next_button = QtGui.QPushButton('Next', self)
		next_button.clicked.connect(self.next_batch)
		next_button.resize(next_button.sizeHint())
		
		prev_button = QtGui.QPushButton('Previous', self)
		prev_button.clicked.connect(self.prev_batch)
		prev_button.resize(prev_button.sizeHint())
		
		rand_button = QtGui.QPushButton('Random', self)
		rand_button.clicked.connect(self.rand_batch)
		rand_button.resize(rand_button.sizeHint())
		
		self.num_label = QtGui.QLabel('%d/%d'%(self.current+1,self.num_batches), self)
		self.num_label.resize(self.num_label.sizeHint())
		
		hbox.addWidget(self.num_label)
		hbox.addStretch(1)
		hbox.addWidget(self.num_entry)
		hbox.addWidget(rand_button)
		hbox.addWidget(prev_button)
		hbox.addWidget(next_button)
		
		vbox.addWidget(self.iw)
		vbox.addLayout(hbox)
		
		self.setLayout(vbox)
		
		self.resize(800,800)
		self.show()
		
		self.get_batch(0)
	
	def get_batch(self, b_num):
		f = h5py.File(sys.argv[1], "r")
		for i in range(self.c_num):
			raw_chunk = f[self.h5path][b_num*self.b_size + i*self.c_size:b_num*self.b_size + (i+1)*self.c_size]
			for j in range(self.c_size):
#				self.raw_images[i*self.c_size + j] = raw_chunk[j].ravel()
				self.images[i*self.c_size + j, self.y, self.x] = raw_chunk[j].ravel()
				sys.stderr.write("\rParsed frame %d/%d in batch %d/%d" %(i*self.c_size+j+1,self.b_size,b_num,self.num_batches))
		f.close()
		
#		images = np.array(map(lambda raw: apply_geom_ij_yx((self.x, self.y), raw), self.raw_images))
		
#		self.iw.setImage(images, autoLevels=False)
		self.iw.setImage(self.images.transpose((0,2,1)), autoLevels=False)
		self.num_label.setText('%d/%d'%(b_num+1,self.num_batches))
	
	def next_batch(self):
		self.current += 1
		if self.current > self.num_batches - 1:
			self.current = self.num_batches - 1
		self.get_batch(self.current)
	
	def prev_batch(self):
		self.current -= 1
		if self.current < 0:
			self.current = 0
		self.get_batch(self.current)
	
	def num_batch(self):
		num = int(self.num_entry.text())
		if num <= self.num_batches and num > 0 and num-1 != self.current:
			self.current = num - 1
			self.get_batch(self.current)
	
	def rand_batch(self):
		self.current = np.random.randint(0, self.num_batches)
		self.get_batch(self.current)

def main():
	app = QtGui.QApplication(sys.argv)
	sh = shotview()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
