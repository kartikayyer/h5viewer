import sys
import glob
import numpy as np
import h5py
from geometry_funcs import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

if len(sys.argv) < 2:
	print "Need cxi file"
	sys.exit()

class shotview(QtGui.QWidget):
	def __init__(self):
		super(shotview, self).__init__()
		
		self.current = 0
		self.b_size = 10
		self.fname = sys.argv[1]
		self.h5path = '/entry_1/data_1/detector_corrected/data'
		self.geom_fname = 'cspad-cxif5315-cxi-taw4.geom'
		self.mjpath = '/LCLS/f_21_ENRC'
		self.npeakspath = 'entry_1/result_1/nPeaks'
		#self.peakxpath = 'entry_1/result_1/peakXPosAssembled'
		#self.peakypath = 'entry_1/result_1/peakYPosAssembled'
		self.peakxpath = 'entry_1/result_1/peakXPosRaw'
		self.peakypath = 'entry_1/result_1/peakYPosRaw'
		self.peaks_flag = False
		self.peakbox = []
		
		self.initdata()
		self.initUI()
	
	def initdata(self):
		f = h5py.File(self.fname, "r")
		self.mj = f[self.mjpath][:]
		self.num_images = len(f[self.h5path])
		self.npeaks = f[self.npeakspath][:]
		f.close()
		
		print "Number of frames =", self.num_images
		self.num_batches = int(self.num_images / self.b_size) + 1
		
		yx, self.shape = get_ij_slab_shaped(self.geom_fname)
		self.y, self.x = yx[0], yx[1]
		
		print "Number of pixels =", len(self.y.ravel())
		
		self.images = np.zeros((self.b_size,) + self.shape, dtype='i2')
	
	def initUI(self):
		hbox = QtGui.QHBoxLayout()
		vbox = QtGui.QVBoxLayout()
		
		self.iw = pg.ImageView()
		self.iw.setLevels(0,200)
		
		self.num_entry = QtGui.QLineEdit('1', self)
		self.num_entry.editingFinished.connect(self.num_batch)
		self.num_entry.resize(self.num_entry.sizeHint())
		
		next_button = QtGui.QPushButton('Next', self)
		next_button.clicked.connect(self.next_batch)
		next_button.resize(next_button.sizeHint())
		
		prev_button = QtGui.QPushButton('Previous', self)
		prev_button.clicked.connect(self.prev_batch)
		prev_button.resize(prev_button.sizeHint())
		
		#rand_button = QtGui.QPushButton('Random', self)
		#rand_button.clicked.connect(self.rand_batch)
		#rand_button.resize(rand_button.sizeHint())
		
		peaks_check = QtGui.QCheckBox('Peaks', self)
		peaks_check.stateChanged.connect(self.toggle_peaks)
		peaks_check.resize(peaks_check.sizeHint())
		
		self.num_slider = QtGui.QSlider(QtCore.Qt.Horizontal, self)
		self.num_slider.setMaximum(self.b_size)
		self.num_slider.valueChanged.connect(self.change_frame)
		self.num_slider.resize(self.num_slider.sizeHint())
		
		self.num_label = QtGui.QLabel('%d/%d'%(self.current+1,self.num_batches), self)
		self.num_label.resize(self.num_label.sizeHint())
		
		hbox.addWidget(self.num_label)
		hbox.addWidget(self.num_entry)
		hbox.addWidget(self.num_slider)
		hbox.addStretch(1)
		hbox.addWidget(peaks_check)
		#hbox.addWidget(rand_button)
		hbox.addWidget(prev_button)
		hbox.addWidget(next_button)
		
		vbox.addWidget(self.iw)
		vbox.addLayout(hbox)
		
		self.setLayout(vbox)
		
		self.resize(800,800)
		self.show()
		
		self.get_batch(0)
	
	def get_batch(self, b_num):
		if b_num == self.num_batches - 1:
			bs = self.num_images - b_num*self.b_size
		else:
			bs = self.b_size
		
		f = h5py.File(self.fname, "r")
		for i in range(bs):
			self.images[i, self.y, self.x] = f[self.h5path][b_num*self.b_size + i].ravel()
			sys.stderr.write("\rParsed frame %d/%d in batch %d/%d" %(i+1,bs,b_num+1,self.num_batches))
		f.close()
		
		self.iw.setImage(self.images[0:bs], autoLevels=False)
		self.num_label.setText('%d/%d'%(b_num+1,self.num_batches))
	
	def next_batch(self):
		self.current += 1
		if self.current > self.num_batches - 1:
			self.current = self.num_batches - 1
		self.get_batch(self.current)
	
	def prev_batch(self):
		self.current -= 1
		if self.current < 0:
			self.current = 0
		self.get_batch(self.current)
	
	def num_batch(self):
		num = int(self.num_entry.text())
		if num <= self.num_batches and num > 0 and num-1 != self.current:
			self.current = num - 1
			self.get_batch(self.current)
	
	def rand_batch(self):
		self.current = np.random.randint(0, self.num_batches)
		self.get_batch(self.current)
	
	def change_frame(self):
		num = self.num_slider.value()
		self.iw.setCurrentIndex(num)
		if self.peaks_flag:
			self.show_peaks(num)
	
	def toggle_peaks(self):
		self.peaks_flag = not self.peaks_flag
		if self.peaks_flag:
			self.show_peaks(self.num_slider.value())
		else:
			self.hide_peaks()
	
	def hide_peaks(self):
		for p in range(len(self.peakbox)):
			self.iw.removeItem(self.peakbox[p])
		self.peakbox = []
	
	def show_peaks(self, n):
		idx = self.current*self.b_size + n
		
		self.hide_peaks()
		
		f = h5py.File(self.fname, "r")
		#px = f[self.peakxpath][idx, 0:self.npeaks[idx]] + self.shape[0]/2
		#py = f[self.peakypath][idx, 0:self.npeaks[idx]] + self.shape[1]/2
		t = np.round(f[self.peakxpath][idx, 0:self.npeaks[idx]]) + 1552*np.round(f[self.peakypath][idx, 0:self.npeaks[idx]])
		f.close()
		
		t = t.astype('int')
		px = self.x[t]
		py = self.y[t]
		
		for p in range(len(px)):
			self.peakbox.append(pg.ROI([py[p]-10,px[p]-10], [20,20], movable=False))
			self.iw.addItem(self.peakbox[p])

def main():
	app = QtGui.QApplication(sys.argv)
	sh = shotview()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
