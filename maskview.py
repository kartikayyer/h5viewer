import sys
import glob
import numpy as np
import h5py
from geometry_funcs import *
from pyqtgraph.Qt import QtCore, QtGui
import pyqtgraph as pg

if len(sys.argv) < 2:
	print "Need file list"
	sys.exit()

class shotview(QtGui.QWidget):
	def __init__(self):
		super(shotview, self).__init__()
		
		self.h5path = '/data/data'
		self.geom_fname = 'cspad-cxif5315-cxi-taw4.geom'
		
		self.initdata()
		self.initUI()
	
	def initdata(self):
		f = h5py.File(sys.argv[1], "r")
		mask = np.array(f[self.h5path]).ravel() + 0.5
		f.close() 
		
		yx, shape = get_ij_slab_shaped(self.geom_fname)
		self.y, self.x = yx[0], yx[1]
		
		self.mask_assem = np.zeros(shape)
		self.mask_assem[self.y, self.x] = mask
		
		print "Number of pixels =", len(self.y.ravel())
		
	def initUI(self):
		vbox = QtGui.QVBoxLayout()
		
		self.iw = pg.ImageView()
		
		vbox.addWidget(self.iw)
		
		self.setLayout(vbox)
		
		self.resize(800,800)
		self.show()
		
		self.view_mask()
	
	def view_mask(self):
		self.iw.setImage(self.mask_assem)

def main():
	app = QtGui.QApplication(sys.argv)
	sh = shotview()
	sys.exit(app.exec_())

if __name__ == '__main__':
	main()
